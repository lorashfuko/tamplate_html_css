var lr = require('tiny-lr'), // Минивебсервер для livereload
	gulp = require('gulp'), // Сообственно Gulp JS
	jade = require('gulp-jade'), // Плагин для Jade
    compass = require('gulp-compass'), // Плагин для Compass
	livereload = require('gulp-livereload'), // Livereload для Gulp
	myth = require('gulp-myth'), // Плагин для Myth - http://www.myth.io/
	csso = require('gulp-csso'), // Минификация CSS
	imagemin = require('gulp-imagemin'), // Минификация изображений
	uglify = require('gulp-uglify'), // Минификация JS
	concat = require('gulp-concat'), // Склейка файлов
	connect = require('connect'), // Webserver
	server = lr();

gulp.task('compass', function() {
	gulp.src('./assets/scss/*.scss')
		.pipe(compass({
			//config_file: './config.rb',
			css: 'www/css/',
			sass: 'assets/scss/'
		}))
        .pipe(myth())
        .pipe(gulp.dest('./www/css/')) ;// записываем css
});

// Собираем html из Jade
gulp.task('jade', function() {
	gulp.src(['./assets/template/*.jade', '!./assets/template/includes/_*.jade'])
		.pipe(jade({
            pretty: '\t'
		}))  // Собираем Jade только в папке ./assets/template/ исключая файлы с _*
		.on('error', console.log) // Если есть ошибки, выводим и продолжаем
		.pipe(gulp.dest('./www/')) // Записываем собранные файлы
});

// Собираем JS
gulp.task('js', function() {
	gulp.src(['./assets/js/**/*.js', '!./assets/js/vendor/**/*.js'])
		.pipe(concat('scripts.js')) // Собираем все JS, кроме тех которые находятся в ./assets/js/vendor/**
		.pipe(gulp.dest('./www/js'))
});

gulp.task('images', function() {
	gulp.src('./assets/img/**/*')
		.pipe(imagemin())
		.pipe(gulp.dest('./www/img'))

});

gulp.task('watch', function() {
	// Предварительная сборка проекта
	gulp.run('compass');
	gulp.run('jade');
	gulp.run('images');
	gulp.run('js');

    gulp.watch('assets/scss/**/*.scss', function() {
        gulp.run('compass');
    });
    gulp.watch('assets/template/**/*.jade', function() {
        gulp.run('jade');
    });
    gulp.watch('assets/img/**/*', function() {
        gulp.run('images');
    });
    gulp.watch('assets/js/**/*', function() {
        gulp.run('js');
    });
});

gulp.task('build', function() {
    // css
    gulp.src('./assets/scss/*.scss')
        .pipe(compass({
            //config_file: './config.rb',
            css: 'www/css/',
            sass: 'assets/scss/'
        }))
        .pipe(myth()) // добавляем префиксы - http://www.myth.io/
        .pipe(csso()) // минимизируем css
        .pipe(gulp.dest('./build/css/')) // записываем css

    // jade
    gulp.src(['./assets/template/*.jade', '!./assets/template/_*.jade'])
        .pipe(jade())
        .pipe(gulp.dest('./build/'))

    // js
    gulp.src(['./assets/js/**/*.js', '!./assets/js/vendor/**/*.js'])
        .pipe(concat('scripts.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./build/js'));

    // image
    gulp.src('./assets/img/**/*')
        .pipe(imagemin())
        .pipe(gulp.dest('./build/img'))

});